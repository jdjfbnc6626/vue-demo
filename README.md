# Clone this project, then:

- I .gitignore , but a common fresh start with JS projects is:

```
rm -rf node_modules
rm -f package-lock.json
npm install
```

- This blows out any wierd state that's left hanging from a local box, and starts you with freshly installed dependencies.
- Then

```
npm run serve
```

- The 'serve" script is inspectable in your package.json. You gotta npm run it.
- Now the Vue app is on your localhost:8000. Make sure to set the port in your browser. Inspect stuff with the F12 key.
